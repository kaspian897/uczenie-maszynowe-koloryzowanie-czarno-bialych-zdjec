import Convolution
import numpy as np
from pytictoc import TicToc
t = TicToc()
in_CH=64
in_W=128
in_H=128
o_CH=512*64
k_W=3
k_H=3
P=3
S=1
W_out = int((in_W - k_W + 2 * P) / S+1);
H_out=int((in_H - k_H + 2 * P) / S+1) ;



input = np.ones(shape=(in_CH,in_W,in_H),dtype=np.float32)
filters = np.ones(shape=(o_CH,in_CH,k_W,k_H),dtype=np.float32) 
result = np.zeros(shape=(o_CH,W_out,H_out),dtype=np.float32) 
#input = np.array([[[1,2,3],[4,5,6],[7,8,9]]],dtype=np.float32)
#filters = np.ones(shape=(o_CH,in_CH,k_W,k_H),dtype=np.float64) 
#filters = np.array([[[[1,2,3],[1,2,3],[1,2,3]]]],dtype=np.float32)

#result = np.zeros(shape=(o_CH,W_out,H_out),dtype=np.float32) 
t.tic()

Convolution.convolveIC_AVX_8x8_MT_TS(result,input, filters, in_CH,in_W,in_H,o_CH,k_W,k_H,P,S,4)  
t.toc(restart=True)	
#np.pad(np.ones(shape=(64,256,256),dtype=np.float32) ,3,constant_values=(0))[3:(512+3)]
#t.toc(restart=True)	
#result2 = np.zeros(shape=(W_out*H_out,in_CH*k_W*k_H),dtype=np.float64) 
#Convolution.im2Col(result2,input, in_CH,in_W,in_H,k_W,k_H,P,S)  



