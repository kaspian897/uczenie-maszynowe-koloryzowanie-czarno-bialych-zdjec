import Convolution
import numpy as np
import math
from pytictoc import TicToc
import os
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from skimage.color import rgb2lab, lab2rgb, rgb2gray

class Model:
	def __init__(self,inputShape):
		self.inputShape=inputShape
		self.layers = []
	def addLayer(self, layer):
		self.layers.append(layer)
		
	def forwardPropagation(self,input):
		output=input
		for i in range(0,len(self.layers)):
			#print("FORWARD FILTERS")
			#print(self.layers[i].filters)
			#print("END FILTERS")
			output=self.layers[i].getFutureMaps(output)
			#print(output)
			#print("Layer: "+str(i))
			#print("END FORWARD")
		return output
	def train(self,input,data,epoch):
		error=0
		for i in range(epoch):
			forward=self.forwardPropagation(input)
			#print("FORWARD")
			#print(forward)
			#print("END FORWARDLAST")
			#print(forward.shape)
			#error=meanSquaredError(data,forward)
			
			error=meanSquaredError(data,forward)
			print(error)
			gradient=meanSquaredErrorGradient(data,forward)
			
		
			for i in range(len(self.layers)-1,-1,-1):
				gradient=self.layers[i].propagate_backward(gradient)
				#print("layer "+str(i))
		return error
	def compile(self):
		inputShape=self.inputShape
		for i in range(0,len(self.layers)):
			self.layers[i].init(inputShape)
			inputShape=self.layers[i].outputShape

class UpSampling2D(object):
	def __init__(self,scale):
		self.scale=scale
		self.inputShape=[]
		self.outputShape=[]
		
		self.filters=[]
		self.out=[]
	def init(self,inputShape):
		self.inputShape=inputShape
		
		self.outputShape=(inputShape[0],inputShape[1]*self.scale,inputShape[2]*self.scale)
	def getFutureMaps(self, matrix):
		new_matrix = np.zeros(shape=self.outputShape,dtype=np.float32)


        #x_ratio = old_W/np.float32(new_W)
        #y_ratio = old_H/np.float32(new_H)

        #px, py = 0, 0
		for k in range(self.inputShape[0]):
			new_matrix[k] = matrix[k].repeat(self.scale, axis=0).repeat(self.scale, axis=1)
            #for i in range(new_W):
                #px = math.floor(i * x_ratio)
                #for j in range(new_H):
                    #py = math.floor(j * y_ratio)
                    #new_matrix[k][i][j] = matrix[k][px][py]
		self.out=new_matrix
		return self.out
	def propagate_backward(self,in_gradient):
		return in_gradient[:,::2,::2]
class Conv2D(object):
	def __init__(self,o_CH,size_filter,stride=1,initializer=None,padding=None,activation='relu'):
		self.kernel_W=size_filter[0]
		self.kernel_H=size_filter[1]
		self.stride=stride
		self.padding=padding
		self.o_CH=o_CH
		
		self.inputShape=[]
		self.outputShape=[]
		self.filters=[]
		
		if activation=='relu':
			self.activation=self.reLu
			self.dactivation=self.d_relu
		if activation=='tanh':
			self.activation=self.tanh
			self.dactivation=self.d_tanh
			
		self.out=[]
		self.out_activation=[]
		self.bias=[]
		self.out_gradient=[]
		
		self.prev_layer=[]
		self.RMSprop=0
	def init(self,inputShape):
		self.inputShape=inputShape
		o_H=int((self.inputShape[1]+2*self.padding-self.kernel_W)/self.stride+1)
		o_W=int((self.inputShape[2]+2*self.padding-self.kernel_H)/self.stride+1)
		self.outputShape=(self.o_CH,o_H,o_W)

		#He uniform initializer, inicializacja poczatkowa filtrow
		in_channels=self.inputShape[0]
		fan_in=in_channels*self.kernel_W*self.kernel_H
		limit=np.sqrt(6/fan_in)
		self.filters=np.random.uniform(low=-limit,high=limit,size=(self.o_CH,in_channels,self.kernel_W,self.kernel_H))
		self.filters=np.array(self.filters,dtype=np.float32)
		
		self.filters=np.ones((self.o_CH,in_channels,self.kernel_W,self.kernel_H),dtype=np.float32)/1000
	def getFutureMaps(self,input):
		
		self.prev_layer=input
		futureMaps=np.zeros(self.outputShape,dtype=np.float32)
		
		Convolution.convolveIC_AVX_8x8_MT_TS(futureMaps,input, self.filters, self.inputShape[0],self.inputShape[1],self.inputShape[2],self.o_CH,self.kernel_W,self.kernel_H,self.padding,self.stride,4)  
		
		self.out=futureMaps
		self.out_activation=self.activation(futureMaps)
		
		self.input=input
		return self.out_activation

	def propagate_backward(self,in_gradient):
		b_gradient_component=self.dactivation(self.out)*in_gradient
		b_gradient_component=np.array(b_gradient_component,dtype=np.float32)
		#bias_gradient=Convolution.convolveIC_AVX_8x8_MT_TS(self.dactivation
		
		
		

		#t = TicToc()
		#print("start")

		#t.tic()
		#for i in range(self.o_CH):
		#	filter=np.array([[b_gradient_component[i]]],dtype=np.float32)
		#	for j in range(self.inputShape[0]):	
		#		output=np.zeros((1,self.kernel_W,self.kernel_H),dtype=np.float32)
		#		input=np.array([self.prev_layer[j]],dtype=np.float32)
		#		Convolution.convolveIC_AVX_8x8_MT_TS(output,input, filter, 1,self.inputShape[1],self.inputShape[2],1,self.outputShape[1],self.outputShape[2],self.padding,self.stride,1)
		#		weight_gradient[i,j]=output
		#t.toc(restart=True)	
		
		#for i in range(self.o_CH):
		if(self.stride>1):
			filter=np.array(b_gradient_component.reshape((b_gradient_component.shape[0],1,b_gradient_component.shape[1],b_gradient_component.shape[2])),dtype=np.float32)
			filterS=np.zeros((filter.shape[0],filter.shape[1],filter.shape[2]*self.stride-1,filter.shape[3]*self.stride-1),dtype=np.float32)
			filterS[:,:,::self.stride,::self.stride]=filter
			filter=filterS
			
			pad=1
		else:
			filter=np.array(b_gradient_component.reshape((b_gradient_component.shape[0],1,b_gradient_component.shape[1],b_gradient_component.shape[2])),dtype=np.float32)
			pad=1
		o_H=int((self.inputShape[1]+2*pad-filter.shape[2])/1+1)
		o_W=int((self.inputShape[2]+2*pad-filter.shape[3])/1+1)
		if(o_H>3):
			pad=0
			o_H=int((self.inputShape[1]+2*pad-filter.shape[2])/1+1)
			o_W=int((self.inputShape[2]+2*pad-filter.shape[3])/1+1)
		if(o_H<3):
			pad=-1
			o_H=int((self.inputShape[1]+1-filter.shape[2])/1+1)
			o_W=int((self.inputShape[2]+1-filter.shape[3])/1+1)
			
		#print(pad)
		#print(str(o_H)+" "+ str(self.outputShape[1]))
		#print(str(o_W)+" "+ str(self.outputShape[2]))
		weight_size=(self.o_CH,self.inputShape[0],o_W,o_H)
		weight_gradient=np.zeros(weight_size,dtype=np.float32)
		#print(weight_gradient.shape)
		for j in range(self.inputShape[0]):	
			output=np.zeros((self.o_CH,o_W,o_H),dtype=np.float32)
			input=np.array([self.prev_layer[j]],dtype=np.float32)
			
			
			if(pad!=-1):
				Convolution.convolveIC_AVX_8x8_MT_TS(output,input, filter, 1,self.inputShape[1],self.inputShape[2],self.o_CH,filter.shape[2],filter.shape[3],pad,1,4)
			else:
				input=np.pad(input,(1,0),constant_values=(0))[1:2]
				Convolution.convolveIC_AVX_8x8_MT_TS(output,input, filter, 1,self.inputShape[1]+1,self.inputShape[2]+1,self.o_CH,filter.shape[2],filter.shape[3],0,1,4)
			weight_gradient[:,j]=output
		#print(weight_gradient)
		#t.toc(restart=True)	
		
		#print("Stop")
		#print(self.filters.shape)
		
		#if(self.stride!=0):
		#	
		padding=self.padding
		if(self.stride>1):
			b_gradient_component_S=np.zeros((b_gradient_component.shape[0],b_gradient_component.shape[1]*self.stride-1,b_gradient_component.shape[2]*self.stride-1),dtype=np.float32)
			b_gradient_component_S[:,::self.stride,::self.stride]=b_gradient_component
			b_gradient_component=b_gradient_component_S
			padding=self.padding

		if(self.inputShape[2]==127):
			padding=2

		rot_filters=np.rot90(np.rot90(self.filters,1,axes=(1,0)),2,axes=(3,1))
		rot_filters=np.rot90(rot_filters,axes=(3,2))
		rot_filters=np.array(rot_filters,dtype=np.float32)
		
		o_H=int((b_gradient_component.shape[1]+2*padding-rot_filters.shape[2])/1+1)
		o_W=int((b_gradient_component.shape[2]+2*padding-rot_filters.shape[3])/1+1)
		if(self.inputShape[2]==254):
			padding=1
			o_H=int((self.inputShape[1]+2-rot_filters.shape[2])/1+1)
			o_W=int((self.inputShape[2]+2-rot_filters.shape[3])/1+1)
		outGradient=np.zeros((rot_filters.shape[0],o_H,o_W),dtype=np.float32)
		#print(outGradient.shape)
		Convolution.convolveIC_AVX_8x8_MT_TS(outGradient,b_gradient_component, rot_filters, b_gradient_component.shape[0],b_gradient_component.shape[1],b_gradient_component.shape[2],rot_filters.shape[0],self.kernel_W,self.kernel_H,padding,1,4)
		#t.toc(restart=True)	
		#print("END LAYER")
		#print(self.inputShape)
		#print(self.outputShape)
		RMSprop_v,RMSprop_e= 0.9,1e-07
		learn_rate=0.0001
		self.RMSprop = RMSprop_v*self.RMSprop + (1- RMSprop_v)*weight_gradient**2
	
		self.filters=self.filters+(learn_rate/np.sqrt(self.RMSprop + RMSprop_e))*weight_gradient
		#learning_rate=0.001, rho=0.9, momentum=0.0, epsilon=1e-07
		#self.filters=self.filters+(learn_rate)*weight_gradient
		#print("UPDATE")
		#print(self.filters)
		#print("weight gradient")
		#print(weight_gradient)
		#if(self.stride>1):
		#	out= np.delete(outGradient,(outGradient.shape[2]-1,outGradient.shape[2]-2),axis=2)
		#	return np.delete(out,(out.shape[1]-1,out.shape[1]-2),axis=1)
		del b_gradient_component
		del filter
		del weight_gradient
		return outGradient
	def reLu(self,X):
		return np.maximum(0,X)
	def d_relu(self,x):
		mask  = (x >0) * 1.0 
		return mask 

	def tanh(self,x):
		return np.tanh(x)
	def d_tanh(self,x):
		return 1 - np.tanh(x) ** 2
def meanSquaredError(arrayA,arrayB):
	return np.square(arrayA.flatten()-arrayB.flatten()).sum()/arrayB.flatten().size
def meanSquaredErrorGradient(arrayA,arrayB):
	return (arrayA-arrayB)*2/arrayA.size
def main():
    #in_CH=64
    #in_W=256
    #in_H=256
    #rge = in_CH* in_H* in_W

    #image = np.arange(rge, dtype = np.float32).reshape(in_CH, in_H, in_W)
    #image = np.random.randint(0, 255, size = (in_CH, in_H, in_W))
    #print(image)

    #Up2D = UpSampling2D()

    #t = TicToc()
    #t.tic()
    #new_image = Up2D.GetFutureMaps(image)
    #t.toc(restart=True)
	X = []
	for filename in os.listdir('Train/'):
		X.append(img_to_array(load_img('Train/'+filename)))
	X = np.array(X,dtype=np.float32)

	# Set up training and test data
	Xtrain = X[:]
	Xtrain = 1.0/255*Xtrain
	
	lab = rgb2lab(Xtrain)
	lab=np.array(lab,dtype=np.float32)
	print(lab.shape)
	m=Model(inputShape=(1,256,256))
	m.addLayer(Conv2D(64, (3, 3), activation='tanh', padding=0))
	m.addLayer(Conv2D(64, (3, 3), activation='relu', padding=1, stride=2))
	m.addLayer(Conv2D(128, (3, 3), activation='relu', padding=0))
	m.addLayer(Conv2D(128, (3, 3), activation='relu', padding=1, stride=2))
	m.addLayer(Conv2D(256, (3, 3), activation='relu', padding=1))
	m.addLayer(Conv2D(256, (3, 3), activation='relu', padding=1, stride=2))
	m.addLayer(Conv2D(512, (3, 3), activation='relu', padding=1))
	m.addLayer(Conv2D(256, (3, 3), activation='relu', padding=1))
	m.addLayer(Conv2D(128, (3, 3), activation='relu', padding=1))
	m.addLayer(UpSampling2D(2))
	m.addLayer(Conv2D(64, (3, 3), activation='relu', padding=1))
	m.addLayer(UpSampling2D(2))
	m.addLayer(Conv2D(32, (3, 3), activation='relu', padding=1))
	m.addLayer(Conv2D(2, (3, 3), activation='tanh', padding=1))
	m.addLayer(UpSampling2D(2))
	m.compile()
	
	limit=1
	randomArray=np.random.uniform(low=-3,high=3,size=(1,256,256))
	randomArrayOut=np.random.uniform(low=-1,high=1,size=(256,32,32))
	#randomArray=np.arange(50).reshape((2,5,5))
	#randomArrayOut=np.arange(8).reshape((2,2,2))
	#print(randomArray)
	#print(randomArrayOut)
	#randomArray=np.ones((2,5,5))
	randomArray=np.ones((1,256,256))
	randomArrayOut=np.ones((2,256,256))/100
	randomArray=np.array(randomArray,dtype=np.float32)
	t = TicToc()
	t.tic()
	output1=m.forwardPropagation(randomArray)
	
	data=np.array([lab[0,:,:,1],lab[0,:,:,2]])/ 128
	print(data.shape)
	input=np.array([lab[0,:,:,0]])
	print(input.shape)
	output=m.train(input,data,1000)
	output2=m.forwardPropagation(randomArray)
	t.toc(restart=True)	
	#print(output1-output2)
main()