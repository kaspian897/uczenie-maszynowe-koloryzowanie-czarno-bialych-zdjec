//#include<iostream>
//#include <time.h> 
//#include <omp.h> //openMP
#include <immintrin.h>//AVX2
//#include <algorithm>
//#include <stdio.h>
#include "Convolution.h"
#include <thread> 
//#include <mutex>   
using namespace std;

float** im2col_3x3(float* A, int in_CH,int in_W,int in_H, int W_out, int H_out, int K_W, int K_H, int* lenghtCol, int* lengthRow,int S) {
	*lenghtCol = (in_CH) * (K_H) * (K_W);
	int r = *lenghtCol % 8;
	if (r) {
		*lenghtCol += (8 - r);
	}

	*lengthRow = (W_out) * (H_out);
	int rw = *lengthRow % 8;
	if (rw) {
		*lengthRow += (8 - rw);
	}

	float** imCol = (float**)malloc(*lengthRow * sizeof(float*));

	for (int j = 0; j < *lengthRow; j++) {

		imCol[j] = (float*)malloc(*lenghtCol * sizeof(float));
	}
	if (r) {
		for (int i = 0; i < *lengthRow; i++) {
			for (int j = *lenghtCol - 1; j >=*lenghtCol - (8 - r); j--) {
				imCol[i][j] = 0;

			}
		}
	}
	if (rw) {
		for (int i = *lengthRow - 1; i >= *lengthRow - (8 - rw); i--) {
			for (int j = 0; j < *lenghtCol; j++) {
				imCol[i][j] = 0;
			}

		}
	}

	const int stepRow= in_W;
	const int stepRow2 = in_W*2;
	int a = 0;
	int b = 0;

	int row = 0;
	int col = 0;
	int z = 0;
	int i = 0;
	
	int stepDown=in_W-(W_out-1)*S+(S-1)*in_W;
	//int stepChanel=(in_H-(H_out-1)*S-1)*in_W;

	while (i < in_CH * in_W * in_H) {
		a = (z) * 9;
		imCol[b][a] = A[i];
		imCol[b][a+1] = A[i+1];
		imCol[b][a+2] = A[i+2];
		imCol[b][a+3] = A[i+ stepRow];
		imCol[b][a+4] = A[i+ stepRow +1];
		imCol[b][a+5] = A[i+ stepRow +2];
		imCol[b][a+6] = A[i+ stepRow2];
		imCol[b][a+7] = A[i+ stepRow2 +1];
		imCol[b][a+8] = A[i+ stepRow2 +2];

		row++;
		b++;
		if (row >= W_out) {
			row = 0;
			col++;

			//i += K_W ;
			//i = W_out*H_out*(z) * 9;
			i += stepDown;
			if (col >= H_out) {
				col = 0;
				z++;
				b = 0;

				i =z*in_W*in_H;
			}
		}
		else {
			i+=S;
		}
		
	}

	return  imCol;
}
float** im2colMain(float* A, int in_CH, int in_W, int in_H, int W_out, int H_out, int K_W, int K_H, int* lenghtCol, int* lengthRow,int S) {
	*lenghtCol = (in_CH) * (K_H) * (K_W);
	int r = *lenghtCol % 8;
	if (r) {
		*lenghtCol += (8 - r);
	}

	*lengthRow = (W_out) * (H_out);
	int rw = *lengthRow % 8;
	if (rw) {
		*lengthRow += (8 - rw);
	}

	float** imCol = (float**)malloc(*lengthRow * sizeof(float*));

	for (int j = 0; j < *lengthRow; j++) {

		imCol[j] = (float*)malloc(*lenghtCol * sizeof(float));
	}
	if (r) {
		for (int i = 0; i < *lengthRow; i++) {
			for (int j = *lenghtCol - 1; j >= *lenghtCol - (8 - r); j--) {
				imCol[i][j] = 0;

			}
		}
	}
	if (rw) {
		for (int i = *lengthRow - 1; i >= *lengthRow - (8 - rw); i--) {
			for (int j = 0; j < *lenghtCol; j++) {
				imCol[i][j] = 0;
			}

		}
	}

	const int stepRow2 = in_W * (K_H-1);
	int a = 0;
	int b = 0;

	int row = 0;
	int col = 0;
	int z = 0;
	int i = 0;

	int stepDown = in_W - (W_out - 1) * S + (S - 1) * in_W;

	while (i < in_CH * in_W * in_H) {
		a = (z) * K_H*K_W;
		int offset = 0;
		for (int j = 0; j < K_H; j++) {
			for (int k = 0; k < K_W; k++) {
				imCol[b][a + j* K_W + k] = A[i + offset + k];
			}
			offset += in_W;
		}


		row++;
		b++;
		if (row >= W_out) {
			row = 0;
			col++;

			//i += K_W;
			i += stepDown;
			if (col >= H_out) {
				col = 0;
				z++;
				b = 0;

				//i += stepRow2;
				i = z * in_W * in_H;
			}
		}
		else {
			i+=S;
			//i++;
		}

	}

	return  imCol;
}
float** im2col(float* A, int in_CH, int in_W, int in_H, int W_out, int H_out, int K_W, int K_H, int* lenghtCol, int* lengthRow,int S) {
	if (K_W == 3 && K_H == 3) {
		return im2col_3x3(A, in_CH, in_W, in_H, W_out, H_out, K_W, K_H, lenghtCol, lengthRow,S);
	}
	return im2colMain(A, in_CH, in_W, in_H, W_out, H_out, K_W, K_H, lenghtCol, lengthRow,S);
}
void im2ColExport(float* res, float* input, int in_CH, int in_W, int in_H, int K_W, int K_H,int P,int S) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - K_W + 2 * P) / S + 1;
	const int H_out = (in_H - K_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	float** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, K_W, K_H, &lenghtCol, &lengthRow,S);

	for (int i = 0; i < W_out * H_out; i++) {
		for (int j = 0; j < (in_CH) * (K_H) * (K_W); j++) {
			res[i * (in_CH) * (K_H) * (K_W)+ j] = imTcol[i][j];
		}
	}
	free(imTcol);

}

float** reshapeFilters(float* Filters, int n_f, int in_CH, int k_H, int k_W) {
	int lenghtCol = (in_CH) * (k_H) * (k_W);
	int r = lenghtCol % 8;
	if (r) {
		lenghtCol += (8 - r);
	}
	float** filters = (float**)malloc(n_f * sizeof(float*));

	for (int j = 0; j < n_f; j++) {

		filters[j] = (float*)malloc(lenghtCol * sizeof(float));
	}
	int b = 0;

	for (int j = 0; j < n_f; j++) {
		
		for (int i = 0; i < in_CH* k_H*k_W; i++) {
			filters[j][i] = Filters[b];	
			b++;
			
		}
	}
	return filters;

}

inline void transpose8_ps(__m256 & row0, __m256 & row1, __m256 & row2, __m256 & row3, __m256 & row4, __m256 & row5, __m256 & row6, __m256 & row7) {
	__m256 __t0, __t1, __t2, __t3, __t4, __t5, __t6, __t7;
	__m256 __tt0, __tt1, __tt2, __tt3, __tt4, __tt5, __tt6, __tt7;
	__t0 = _mm256_unpacklo_ps(row0, row1);
	__t1 = _mm256_unpackhi_ps(row0, row1);
	__t2 = _mm256_unpacklo_ps(row2, row3);
	__t3 = _mm256_unpackhi_ps(row2, row3);
	__t4 = _mm256_unpacklo_ps(row4, row5);
	__t5 = _mm256_unpackhi_ps(row4, row5);
	__t6 = _mm256_unpacklo_ps(row6, row7);
	__t7 = _mm256_unpackhi_ps(row6, row7);
	__tt0 = _mm256_shuffle_ps(__t0, __t2, _MM_SHUFFLE(1, 0, 1, 0));
	__tt1 = _mm256_shuffle_ps(__t0, __t2, _MM_SHUFFLE(3, 2, 3, 2));
	__tt2 = _mm256_shuffle_ps(__t1, __t3, _MM_SHUFFLE(1, 0, 1, 0));
	__tt3 = _mm256_shuffle_ps(__t1, __t3, _MM_SHUFFLE(3, 2, 3, 2));
	__tt4 = _mm256_shuffle_ps(__t4, __t6, _MM_SHUFFLE(1, 0, 1, 0));
	__tt5 = _mm256_shuffle_ps(__t4, __t6, _MM_SHUFFLE(3, 2, 3, 2));
	__tt6 = _mm256_shuffle_ps(__t5, __t7, _MM_SHUFFLE(1, 0, 1, 0));
	__tt7 = _mm256_shuffle_ps(__t5, __t7, _MM_SHUFFLE(3, 2, 3, 2));
	row0 = _mm256_permute2f128_ps(__tt0, __tt4, 0x20);
	row1 = _mm256_permute2f128_ps(__tt1, __tt5, 0x20);
	row2 = _mm256_permute2f128_ps(__tt2, __tt6, 0x20);
	row3 = _mm256_permute2f128_ps(__tt3, __tt7, 0x20);
	row4 = _mm256_permute2f128_ps(__tt0, __tt4, 0x31);
	row5 = _mm256_permute2f128_ps(__tt1, __tt5, 0x31);
	row6 = _mm256_permute2f128_ps(__tt2, __tt6, 0x31);
	row7 = _mm256_permute2f128_ps(__tt3, __tt7, 0x31);
}
void transposeMatrix_8x8(float* matrix) {
	__m256 x1 = _mm256_load_ps(&matrix[0]);
	__m256 x2 = _mm256_load_ps(&matrix[8]);
	__m256 x3 = _mm256_load_ps(&matrix[16]);
	__m256 x4 = _mm256_load_ps(&matrix[24]);
	__m256 x5 = _mm256_load_ps(&matrix[32]);
	__m256 x6 = _mm256_load_ps(&matrix[40]);
	__m256 x7 = _mm256_load_ps(&matrix[48]);
	__m256 x8 = _mm256_load_ps(&matrix[56]);
	transpose8_ps(x1, x2, x3, x4, x5, x6, x7, x8);
	_mm256_store_ps(&matrix[0], x1);
	_mm256_store_ps(&matrix[8], x2);
	_mm256_store_ps(&matrix[16], x3);
	_mm256_store_ps(&matrix[24], x4);
	_mm256_store_ps(&matrix[32], x5);
	_mm256_store_ps(&matrix[40], x6);
	_mm256_store_ps(&matrix[48], x7);
	_mm256_store_ps(&matrix[56], x8);

}

void convolveSimple(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S) {
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	for (int filter = 0; filter < o_CH; filter++) {
		for (int i = 0; i < in_CH; i++) {
			for (int j = 0; j < W_out; j++) {
				for (int k = 0; k < H_out; k++) {
					for (int ki = 0; ki < k_W; ki++) {
						for (int kj = 0; kj < k_H; kj++) {
							res[filter * W_out * H_out + j * W_out + k] += input[i * in_H * in_W + (j + ki) * in_W + k + kj] * Filters[filter * k_W * k_H + ki * k_W + kj];
						}
					}
				}
			}
		}
	}
}
void convolveSimple_MultiThread(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S,int N_thread) {

	auto f = [](float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S,int N_thread,int Thread_ID) {
		const int W_out = (in_W - k_W + 2 * P) / S + 1;
		const int H_out = (in_H - k_H + 2 * P) / S + 1;

		for (int filter = Thread_ID; filter < o_CH; filter+= N_thread) {
			for (int i = 0; i < in_CH; i++) {
				for (int j = 0; j < W_out; j++) {
					for (int k = 0; k < H_out; k++) {
						for (int ki = 0; ki < k_W; ki++) {
							for (int kj = 0; kj < k_H; kj++) {
								res[filter * W_out * H_out + j * W_out + k] += input[i * in_H * in_W + (j + ki) * in_W + k + kj] * Filters[filter * k_W * k_H + ki * k_W + kj];
							}
						}
					}
				}
			}
		}
	};
	thread *th=new thread[N_thread];
	for (int i = 0; i < N_thread; i++) {
		th[i] = thread(f, res, input, Filters, in_CH, in_W, in_H, o_CH, k_W, k_H, P, S, N_thread,i);
	}
	for (int i = 0; i < N_thread; i++) {
		th[i].join();
	}
}
void convolve_Im2col_Simple(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	float** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow,S);
	float** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);

	for (int i = 0; i < o_CH; i++) {
		for (int j = 0; j < lengthRow; j++) {
			for (int k = 0; k < lenghtCol; k++) {
				res[i* W_out*H_out+ j ] +=  imTcol[j][k]*filters[i][k];
			}
		}
	}
	free(imTcol);
	free(filters);
}
void convolve_Im2col_Simple_MultiThread(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S,int N_thread) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	float** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow, S);
	float** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);
	auto f = [](float* res, float** imTcol, float** filters, int o_CH, int W_out, int H_out, int lengthRow, int lenghtCol, int N_thread, int Thread_ID) {
		for (int i = Thread_ID; i < o_CH; i+= N_thread) {
			for (int j = 0; j < lengthRow; j++) {
				for (int k = 0; k < lenghtCol; k++) {
					res[i * W_out * H_out + j] += imTcol[j][k] * filters[i][k];
				}
			}
		}
	};
	thread* th = new thread[N_thread];
	for (int i = 0; i < N_thread; i++) {
		th[i] = thread(f, res, imTcol, filters, o_CH, W_out, H_out, lengthRow, lenghtCol, N_thread, i);
	}
	for (int i = 0; i < N_thread; i++) {
		th[i].join();
	}
	free(imTcol);
	free(filters);
}
void convolve_Im2col_simple_SIMD_AVX(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	float** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow, S);
	float** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);

	for (int i = 0; i < o_CH; i++) {
		for (int j = 0; j < lengthRow; j++) {
			__m256 x;
			__m256 y;
			__m256 acc;
			acc = _mm256_setzero_ps();
			for (int k = 0; k < lenghtCol; k+=8) {
				x = _mm256_load_ps(&filters[i][k]);
				y = _mm256_load_ps(&imTcol[j][k]);
				acc = _mm256_fmadd_ps(x, y, acc);
				//_mm256_store_ps(&res[i * W_out * H_out + j], acc);
				//res[i * W_out * H_out + j] += imTcol[j][k] * filters[i][k];
			}
			res[i * W_out * H_out + j] = acc.m256_f32[0] + acc.m256_f32[1] + acc.m256_f32[2] + acc.m256_f32[3] + acc.m256_f32[4] + acc.m256_f32[5] + acc.m256_f32[6] + acc.m256_f32[7];
		}
	}
	free(imTcol);
	free(filters);
}
void convolve_Im2col_simple_SIMD_AVX_MultiThread(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	float** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow, S);
	float** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);

	auto f = [](float* res, float** imTcol, float** filters, int o_CH, int W_out, int H_out, int lengthRow, int lenghtCol, int N_thread, int Thread_ID) {
		for (int i = Thread_ID; i < o_CH; i+= N_thread) {
			for (int j = 0; j < lengthRow; j++) {
				__m256 x;
				__m256 y;
				__m256 acc;
				acc = _mm256_setzero_ps();
				for (int k = 0; k < lenghtCol; k += 8) {
					x = _mm256_load_ps(&filters[i][k]);
					y = _mm256_load_ps(&imTcol[j][k]);
					acc = _mm256_fmadd_ps(x, y, acc);
					//_mm256_store_ps(&res[i * W_out * H_out + j], acc);
					//res[i * W_out * H_out + j] += imTcol[j][k] * filters[i][k];
				}
				res[i * W_out * H_out + j] = acc.m256_f32[0] + acc.m256_f32[1] + acc.m256_f32[2] + acc.m256_f32[3] + acc.m256_f32[4] + acc.m256_f32[5] + acc.m256_f32[6] + acc.m256_f32[7];
			}
		}
	};
	thread* th = new thread[N_thread];
	for (int i = 0; i < N_thread; i++) {
		th[i] = thread(f, res, imTcol, filters, o_CH, W_out, H_out, lengthRow, lenghtCol, N_thread, i);
	}
	for (int i = 0; i < N_thread; i++) {
		th[i].join();
	}
	free(imTcol);
	free(filters);
}
float sum8(__m256 x) {
	// hiQuad = ( x7, x6, x5, x4 )
	const __m128 hiQuad = _mm256_extractf128_ps(x, 1);
	// loQuad = ( x3, x2, x1, x0 )
	const __m128 loQuad = _mm256_castps256_ps128(x);
	// sumQuad = ( x3 + x7, x2 + x6, x1 + x5, x0 + x4 )
	const __m128 sumQuad = _mm_add_ps(loQuad, hiQuad);
	// loDual = ( -, -, x1 + x5, x0 + x4 )
	const __m128 loDual = sumQuad;
	// hiDual = ( -, -, x3 + x7, x2 + x6 )
	const __m128 hiDual = _mm_movehl_ps(sumQuad, sumQuad);
	// sumDual = ( -, -, x1 + x3 + x5 + x7, x0 + x2 + x4 + x6 )
	const __m128 sumDual = _mm_add_ps(loDual, hiDual);
	// lo = ( -, -, -, x0 + x2 + x4 + x6 )
	const __m128 lo = sumDual;
	// hi = ( -, -, -, x1 + x3 + x5 + x7 )
	const __m128 hi = _mm_shuffle_ps(sumDual, sumDual, 0x1);
	// sum = ( -, -, -, x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7 )
	const __m128 sum = _mm_add_ss(lo, hi);
	return _mm_cvtss_f32(sum);
}
void convolve_Im2col_simple_SIMD_AVX_MultiThread_2_8(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	float** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow, S);
	float** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);

	auto f = [](float* res, float** imTcol, float** filters, int o_CH, int W_out, int H_out, int lengthRow, int lenghtCol, int N_thread, int Thread_ID) {
		for (int i = Thread_ID; i < o_CH; i += N_thread) {
			for (int j = 0; j < lengthRow; j += 2) {
				__m256 x;
				__m256 y;
				__m256 y2;
				__m256 acc;
				__m256 acc2;
				acc = _mm256_setzero_ps();
				acc2 = _mm256_setzero_ps();
				for (int k = 0; k < lenghtCol; k += 8) {
					x = _mm256_load_ps(&filters[i][k]);
					y = _mm256_load_ps(&imTcol[j][k]);
					y2 = _mm256_load_ps(&imTcol[j + 1][k]);
					acc = _mm256_fmadd_ps(x, y, acc);
					acc2 = _mm256_fmadd_ps(x, y2, acc2);
					//_mm256_store_ps(&res[i * W_out * H_out + j], acc);
					//res[i * W_out * H_out + j] += imTcol[j][k] * filters[i][k];
				}
				res[i * W_out * H_out + j] = acc.m256_f32[0] + acc.m256_f32[1] + acc.m256_f32[2] + acc.m256_f32[3] + acc.m256_f32[4] + acc.m256_f32[5] + acc.m256_f32[6] + acc.m256_f32[7];
				if (j + 1 < W_out * H_out) {
					res[i * W_out * H_out + j + 1] = acc2.m256_f32[0] + acc2.m256_f32[1] + acc2.m256_f32[2] + acc2.m256_f32[3] + acc2.m256_f32[4] + acc2.m256_f32[5] + acc2.m256_f32[6] + acc2.m256_f32[7];
				}
			}
		}
	};
	thread* th = new thread[N_thread];
	for (int i = 0; i < N_thread; i++) {
		th[i] = thread(f, res, imTcol, filters, o_CH, W_out, H_out, lengthRow, lenghtCol, N_thread, i);
	}
	for (int i = 0; i < N_thread; i++) {
		th[i].join();
	}
	free(imTcol);
	free(filters);
}
void convolve_Im2col_simple_SIMD_AVX_MultiThread_2_8_fastSUM(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	float** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow, S);
	float** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);

	auto f = [](float* res, float** imTcol, float** filters, int o_CH, int W_out, int H_out, int lengthRow, int lenghtCol, int N_thread, int Thread_ID) {
		for (int i = Thread_ID; i < o_CH; i += N_thread) {
			for (int j = 0; j < lengthRow; j += 2) {
				__m256 x;
				__m256 y;
				__m256 y2;
				__m256 acc;
				__m256 acc2;
				acc = _mm256_setzero_ps();
				acc2 = _mm256_setzero_ps();
				for (int k = 0; k < lenghtCol; k += 8) {
					x = _mm256_load_ps(&filters[i][k]);
					y = _mm256_load_ps(&imTcol[j][k]);
					y2 = _mm256_load_ps(&imTcol[j + 1][k]);
					acc = _mm256_fmadd_ps(x, y, acc);
					acc2 = _mm256_fmadd_ps(x, y2, acc2);

				}
				res[i * W_out * H_out + j] = sum8(acc);

				if (j + 1 < W_out * H_out) {
					res[i * W_out * H_out + j + 1] = sum8(acc2);
				}
			}
		}
	};
	thread* th = new thread[N_thread];
	for (int i = 0; i < N_thread; i++) {
		th[i] = thread(f, res, imTcol, filters, o_CH, W_out, H_out, lengthRow, lenghtCol, N_thread, i);
	}
	for (int i = 0; i < N_thread; i++) {
		th[i].join();
	}
	free(imTcol);
	free(filters);
}

void convolve_Im2col_simple_SIMD_AVX_MultiThread_4_8_fastSUM(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	float** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow, S);
	float** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);

	auto f = [](float* res, float** imTcol, float** filters, int o_CH, int W_out, int H_out, int lengthRow, int lenghtCol, int N_thread, int Thread_ID) {
		for (int i = Thread_ID; i < o_CH; i += N_thread) {
			for (int j = 0; j < lengthRow; j += 4) {
				__m256 x;
				__m256 y;
				__m256 y2;
				__m256 y3;
				__m256 y4;
				__m256 acc;
				__m256 acc2;
				__m256 acc3;
				__m256 acc4;
				acc = _mm256_setzero_ps();
				acc2 = _mm256_setzero_ps();
				acc3 = _mm256_setzero_ps();
				acc4 = _mm256_setzero_ps();
				for (int k = 0; k < lenghtCol; k += 8) {
					x = _mm256_load_ps(&filters[i][k]);
					y = _mm256_load_ps(&imTcol[j][k]);
					y2 = _mm256_load_ps(&imTcol[j + 1][k]);
					y3 = _mm256_load_ps(&imTcol[j + 2][k]);
					y4 = _mm256_load_ps(&imTcol[j + 3][k]);
					acc = _mm256_fmadd_ps(x, y, acc);
					acc2 = _mm256_fmadd_ps(x, y2, acc2);
					acc3 = _mm256_fmadd_ps(x, y3, acc3);
					acc4 = _mm256_fmadd_ps(x, y4, acc4);

				}
				res[i * W_out * H_out + j] = sum8(acc);
				if (j + 3 < W_out * H_out) {
					res[i * W_out * H_out + j + 3] = sum8(acc4);
					res[i * W_out * H_out + j + 2] = sum8(acc3);
					res[i * W_out * H_out + j + 1] = sum8(acc2);
				}
				else if (j + 2< W_out * H_out) {
					res[i * W_out * H_out + j + 2] = sum8(acc3);
					res[i * W_out * H_out + j + 1] = sum8(acc2);
				}
				else if (j + 1 < W_out * H_out) {
					res[i * W_out * H_out + j + 1] = sum8(acc2);
				}
	

			}
		}
	};
	thread* th = new thread[N_thread];
	for (int i = 0; i < N_thread; i++) {
		th[i] = thread(f, res, imTcol, filters, o_CH, W_out, H_out, lengthRow, lenghtCol, N_thread, i);
	}
	for (int i = 0; i < N_thread; i++) {
		th[i].join();
	}
	free(imTcol);
	free(filters);
}

void convolve_Im2col_simple_SIMD_AVX_MultiThread_8_8_fastSUM(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	float** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow, S);
	float** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);

	auto f = [](float* res, float** imTcol, float** filters, int o_CH, int W_out, int H_out, int lengthRow, int lenghtCol, int N_thread, int Thread_ID) {
		for (int i = Thread_ID; i < o_CH; i += N_thread) {
			for (int j = 0; j < lengthRow; j += 8) {
				__m256 x;
				__m256 y1;
				__m256 y2;
				__m256 y3;
				__m256 y4;
				__m256 y5;
				__m256 y6;
				__m256 y7;
				__m256 y8;
				__m256 acc1;
				__m256 acc2;
				__m256 acc3;
				__m256 acc4;
				__m256 acc5;
				__m256 acc6;
				__m256 acc7;
				__m256 acc8;
				acc1 = _mm256_setzero_ps();
				acc2 = _mm256_setzero_ps();
				acc3 = _mm256_setzero_ps();
				acc4 = _mm256_setzero_ps();
				acc5 = _mm256_setzero_ps();
				acc6 = _mm256_setzero_ps();
				acc7 = _mm256_setzero_ps();
				acc8 = _mm256_setzero_ps();
				for (int k = 0; k < lenghtCol; k += 8) {
					x = _mm256_load_ps(&filters[i][k]);
					y1 = _mm256_load_ps(&imTcol[j][k]);
					y2 = _mm256_load_ps(&imTcol[j + 1][k]);
					y3 = _mm256_load_ps(&imTcol[j + 2][k]);
					y4 = _mm256_load_ps(&imTcol[j + 3][k]);
					y5 = _mm256_load_ps(&imTcol[j + 4][k]);
					y6 = _mm256_load_ps(&imTcol[j + 5][k]);
					y7 = _mm256_load_ps(&imTcol[j + 6][k]);
					y8 = _mm256_load_ps(&imTcol[j + 7][k]);
					acc1 = _mm256_fmadd_ps(x, y1, acc1);
					acc2 = _mm256_fmadd_ps(x, y2, acc2);
					acc3 = _mm256_fmadd_ps(x, y3, acc3);
					acc4 = _mm256_fmadd_ps(x, y4, acc4);
					acc5 = _mm256_fmadd_ps(x, y5, acc5);
					acc6 = _mm256_fmadd_ps(x, y6, acc6);
					acc7 = _mm256_fmadd_ps(x, y7, acc7);
					acc8 = _mm256_fmadd_ps(x, y8, acc8);

				}

				res[i * W_out * H_out + j] = sum8(acc1);
				if (j + 7 < W_out * H_out) {
					res[i * W_out * H_out + j + 7] = sum8(acc8);
					res[i * W_out * H_out + j + 6] = sum8(acc7);
					res[i * W_out * H_out + j + 5] = sum8(acc6);
					res[i * W_out * H_out + j + 4] = sum8(acc5);
					res[i * W_out * H_out + j + 3] = sum8(acc4);
					res[i * W_out * H_out + j + 2] = sum8(acc3);
					res[i * W_out * H_out + j + 1] = sum8(acc2);
				}
				else if (j + 6 < W_out * H_out) {
					res[i * W_out * H_out + j + 6] = sum8(acc7);
					res[i * W_out * H_out + j + 5] = sum8(acc6);
					res[i * W_out * H_out + j + 4] = sum8(acc5);
					res[i * W_out * H_out + j + 3] = sum8(acc4);
					res[i * W_out * H_out + j + 2] = sum8(acc3);
					res[i * W_out * H_out + j + 1] = sum8(acc2);
				}
				else if (j + 5 < W_out * H_out) {
					res[i * W_out * H_out + j + 5] = sum8(acc6);
					res[i * W_out * H_out + j + 4] = sum8(acc5);
					res[i * W_out * H_out + j + 3] = sum8(acc4);
					res[i * W_out * H_out + j + 2] = sum8(acc3);
					res[i * W_out * H_out + j + 1] = sum8(acc2);
				}
				else if (j + 4 < W_out * H_out) {
					res[i * W_out * H_out + j + 4] = sum8(acc5);
					res[i * W_out * H_out + j + 3] = sum8(acc4);
					res[i * W_out * H_out + j + 2] = sum8(acc3);
					res[i * W_out * H_out + j + 1] = sum8(acc2);
				}
				else if (j + 3 < W_out * H_out) {
					res[i * W_out * H_out + j + 3] = sum8(acc4);
					res[i * W_out * H_out + j + 2] = sum8(acc3);
					res[i * W_out * H_out + j + 1] = sum8(acc2);
				}
				else if (j + 2 < W_out * H_out) {
					res[i * W_out * H_out + j + 2] = sum8(acc3);
					res[i * W_out * H_out + j + 1] = sum8(acc2);
				}
				else if (j + 1 < W_out * H_out) {
					res[i * W_out * H_out + j + 1] = sum8(acc2);
				}


				//if (j + 1 < W_out * H_out) {
				//	res[i * W_out * H_out + j + 1] = sum8(acc2);
				//}
				//if (j + 2 < W_out * H_out) {
				//	res[i * W_out * H_out + j + 2] = sum8(acc3);
				//}

				//if (j + 3 < W_out * H_out) {
				//	res[i * W_out * H_out + j + 3] = sum8(acc4);
				//}

				//if (j + 4 < W_out * H_out) {
				//	res[i * W_out * H_out + j + 4] = sum8(acc5);
				//}

				//if (j + 5 < W_out * H_out) {
				//	res[i * W_out * H_out + j + 5] = sum8(acc6);
				//}

				//if (j + 6 < W_out * H_out) {
				//	res[i * W_out * H_out + j + 6] = sum8(acc7);
				//}

				//if (j + 7 < W_out * H_out) {
				//	res[i * W_out * H_out + j + 7] = sum8(acc8);

				//}
				//res[i * W_out * H_out + j + 2] = sum8(acc3); 
				//res[i * W_out * H_out + j + 3] = sum8(acc4);
				//res[i * W_out * H_out + j + 4] = sum8(acc5);
				//res[i * W_out * H_out + j + 5] = sum8(acc6);
				//res[i * W_out * H_out + j + 6] = sum8(acc7);
				//res[i * W_out * H_out + j + 7] = sum8(acc8);

			}
		}
	};
	thread* th = new thread[N_thread];
	for (int i = 0; i < N_thread; i++) {
		th[i] = thread(f, res, imTcol, filters, o_CH, W_out, H_out, lengthRow, lenghtCol, N_thread, i);
	}
	for (int i = 0; i < N_thread; i++) {
		th[i].join();
	}
	free(imTcol);
	free(filters);
}

void convolve_Im2col_simple_SIMD_AVX_MultiThread_8_8_TransposeSUM(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	float** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow, S);
	float** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);

	auto f = [](float* res, float** imTcol, float** filters, int o_CH, int W_out, int H_out, int lengthRow, int lenghtCol, int N_thread, int Thread_ID) {
		for (int i = Thread_ID; i < o_CH; i += N_thread) {
			for (int j = 0; j < lengthRow; j += 8) {
				__m256 x;
				__m256 y1;
				__m256 y2;
				__m256 y3;
				__m256 y4;
				__m256 y5;
				__m256 y6;
				__m256 y7;
				__m256 y8;
				__m256 acc1;
				__m256 acc2;
				__m256 acc3;
				__m256 acc4;
				__m256 acc5;
				__m256 acc6;
				__m256 acc7;
				__m256 acc8;
				acc1 = _mm256_setzero_ps();
				acc2 = _mm256_setzero_ps();
				acc3 = _mm256_setzero_ps();
				acc4 = _mm256_setzero_ps();
				acc5 = _mm256_setzero_ps();
				acc6 = _mm256_setzero_ps();
				acc7 = _mm256_setzero_ps();
				acc8 = _mm256_setzero_ps();
				for (int k = 0; k < lenghtCol; k += 8) {
					x = _mm256_load_ps(&filters[i][k]);
					y1 = _mm256_load_ps(&imTcol[j][k]);
					y2 = _mm256_load_ps(&imTcol[j + 1][k]);
					y3 = _mm256_load_ps(&imTcol[j + 2][k]);
					y4 = _mm256_load_ps(&imTcol[j + 3][k]);
					y5 = _mm256_load_ps(&imTcol[j + 4][k]);
					y6 = _mm256_load_ps(&imTcol[j + 5][k]);
					y7 = _mm256_load_ps(&imTcol[j + 6][k]);
					y8 = _mm256_load_ps(&imTcol[j + 7][k]);
					acc1 = _mm256_fmadd_ps(x, y1, acc1);
					acc2 = _mm256_fmadd_ps(x, y2, acc2);
					acc3 = _mm256_fmadd_ps(x, y3, acc3);
					acc4 = _mm256_fmadd_ps(x, y4, acc4);
					acc5 = _mm256_fmadd_ps(x, y5, acc5);
					acc6 = _mm256_fmadd_ps(x, y6, acc6);
					acc7 = _mm256_fmadd_ps(x, y7, acc7);
					acc8 = _mm256_fmadd_ps(x, y8, acc8);

				}

				transpose8_ps(acc1, acc2, acc3, acc4, acc5, acc6, acc7, acc8);
				acc1 = _mm256_add_ps(acc1, acc2);
				acc1 = _mm256_add_ps(acc1, acc3);
				acc1 = _mm256_add_ps(acc1, acc4);
				acc1 = _mm256_add_ps(acc1, acc5);
				acc1 = _mm256_add_ps(acc1, acc6);
				acc1 = _mm256_add_ps(acc1, acc7);
				acc1 = _mm256_add_ps(acc1, acc8);
				

				int l = j + 8 - W_out * H_out;
				if (l<0) {
					_mm256_store_ps(&res[i * W_out * H_out + j], acc1);
				}
				else {
					for (int k = 0; k < 8-l; k++) {
						res[i * W_out * H_out + j + k] = acc1.m256_f32[k];

					}
				}

			}
		}
	};
	thread* th = new thread[N_thread];
	for (int i = 0; i < N_thread; i++) {
		th[i] = thread(f, res, imTcol, filters, o_CH, W_out, H_out, lengthRow, lenghtCol, N_thread, i);
	}
	for (int i = 0; i < N_thread; i++) {
		th[i].join();
	}
	free(imTcol);
	free(filters);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////
//DOUBLE
////////////////////////////////////////////////////////////////////////////////////////////////////////

double** im2col_3x3(double* A, int in_CH, int in_W, int in_H, int W_out, int H_out, int K_W, int K_H, int* lenghtCol, int* lengthRow, int S) {
	*lenghtCol = (in_CH) * (K_H) * (K_W);
	int r = *lenghtCol % 8;
	if (r) {
		*lenghtCol += (8 - r);
	}

	*lengthRow = (W_out) * (H_out);
	int rw = *lengthRow % 8;
	if (rw) {
		*lengthRow += (8 - rw);
	}

	double** imCol = (double**)malloc(*lengthRow * sizeof(double*));

	for (int j = 0; j < *lengthRow; j++) {

		imCol[j] = (double*)malloc(*lenghtCol * sizeof(double));
	}
	if (r) {
		for (int i = 0; i < *lengthRow; i++) {
			for (int j = *lenghtCol - 1; j >= *lenghtCol - (8 - r); j--) {
				imCol[i][j] = 0;

			}
		}
	}
	if (rw) {
		for (int i = *lengthRow - 1; i >= *lengthRow - (8 - rw); i--) {
			for (int j = 0; j < *lenghtCol; j++) {
				imCol[i][j] = 0;
			}

		}
	}

	const int stepRow = in_W;
	const int stepRow2 = in_W * 2;
	int a = 0;
	int b = 0;

	int row = 0;
	int col = 0;
	int z = 0;
	int i = 0;

	int stepDown = in_W - (W_out - 1) * S + (S - 1) * in_W;
	//int stepChanel=(in_H-(H_out-1)*S-1)*in_W;

	while (i < in_CH * in_W * in_H) {
		a = (z) * 9;
		imCol[b][a] = A[i];
		imCol[b][a + 1] = A[i + 1];
		imCol[b][a + 2] = A[i + 2];
		imCol[b][a + 3] = A[i + stepRow];
		imCol[b][a + 4] = A[i + stepRow + 1];
		imCol[b][a + 5] = A[i + stepRow + 2];
		imCol[b][a + 6] = A[i + stepRow2];
		imCol[b][a + 7] = A[i + stepRow2 + 1];
		imCol[b][a + 8] = A[i + stepRow2 + 2];

		row++;
		b++;
		if (row >= W_out) {
			row = 0;
			col++;

			//i += K_W ;
			//i = W_out*H_out*(z) * 9;
			i += stepDown;
			if (col >= H_out) {
				col = 0;
				z++;
				b = 0;

				i = z * in_W * in_H;
			}
		}
		else {
			i += S;
		}

	}

	return  imCol;
}
double** im2colMain(double* A, int in_CH, int in_W, int in_H, int W_out, int H_out, int K_W, int K_H, int* lenghtCol, int* lengthRow, int S) {
	*lenghtCol = (in_CH) * (K_H) * (K_W);
	int r = *lenghtCol % 8;
	if (r) {
		*lenghtCol += (8 - r);
	}

	*lengthRow = (W_out) * (H_out);
	int rw = *lengthRow % 8;
	if (rw) {
		*lengthRow += (8 - rw);
	}

	double** imCol = (double**)malloc(*lengthRow * sizeof(double*));

	for (int j = 0; j < *lengthRow; j++) {

		imCol[j] = (double*)malloc(*lenghtCol * sizeof(double));
	}
	if (r) {
		for (int i = 0; i < *lengthRow; i++) {
			for (int j = *lenghtCol - 1; j >= *lenghtCol - (8 - r); j--) {
				imCol[i][j] = 0;

			}
		}
	}
	if (rw) {
		for (int i = *lengthRow - 1; i >= *lengthRow - (8 - rw); i--) {
			for (int j = 0; j < *lenghtCol; j++) {
				imCol[i][j] = 0;
			}

		}
	}

	const int stepRow2 = in_W * (K_H - 1);
	int a = 0;
	int b = 0;

	int row = 0;
	int col = 0;
	int z = 0;
	int i = 0;

	int stepDown = in_W - (W_out - 1) * S + (S - 1) * in_W;

	while (i < in_CH * in_W * in_H) {
		a = (z)*K_H * K_W;
		int offset = 0;
		for (int j = 0; j < K_H; j++) {
			for (int k = 0; k < K_W; k++) {
				imCol[b][a + j * K_W + k] = A[i + offset + k];
			}
			offset += in_W;
		}


		row++;
		b++;
		if (row >= W_out) {
			row = 0;
			col++;

			//i += K_W;
			i += stepDown;
			if (col >= H_out) {
				col = 0;
				z++;
				b = 0;

				//i += stepRow2;
				i = z * in_W * in_H;
			}
		}
		else {
			i += S;
			//i++;
		}

	}

	return  imCol;
}
double** im2col(double* A, int in_CH, int in_W, int in_H, int W_out, int H_out, int K_W, int K_H, int* lenghtCol, int* lengthRow, int S) {
	if (K_W == 3 && K_H == 3) {
		return im2col_3x3(A, in_CH, in_W, in_H, W_out, H_out, K_W, K_H, lenghtCol, lengthRow, S);
	}
	return im2colMain(A, in_CH, in_W, in_H, W_out, H_out, K_W, K_H, lenghtCol, lengthRow, S);
}
void im2ColExport(double* res, float* input, int in_CH, int in_W, int in_H, int K_W, int K_H, int P, int S) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - K_W + 2 * P) / S + 1;
	const int H_out = (in_H - K_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	float** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, K_W, K_H, &lenghtCol, &lengthRow, S);

	for (int i = 0; i < W_out * H_out; i++) {
		for (int j = 0; j < (in_CH) * (K_H) * (K_W); j++) {
			res[i * (in_CH) * (K_H) * (K_W)+j] = imTcol[i][j];
		}
	}
	free(imTcol);

}

inline double sum8_double(__m256d v) {
	__m128d vlow = _mm256_castpd256_pd128(v);
	__m128d vhigh = _mm256_extractf128_pd(v, 1); // high 128
	vlow = _mm_add_pd(vlow, vhigh);     // reduce down to 128

	__m128d high64 = _mm_unpackhi_pd(vlow, vlow);
	return  _mm_cvtsd_f64(_mm_add_sd(vlow, high64));  // reduce to scalar
}

inline void transpose8_pd(__m256d& row0, __m256d& row1, __m256d& row2, __m256d& row3) {                               
	__m256d tmp3, tmp2, tmp1, tmp0;                              
		
	tmp0 = _mm256_shuffle_pd((row0), (row1), 0x0);                    
	tmp2 = _mm256_shuffle_pd((row0), (row1), 0xF);                
	tmp1 = _mm256_shuffle_pd((row2), (row3), 0x0);                    
	tmp3 = _mm256_shuffle_pd((row2), (row3), 0xF);                
		
	row0 = _mm256_permute2f128_pd(tmp0, tmp1, 0x20);   
	row1 = _mm256_permute2f128_pd(tmp2, tmp3, 0x20);   
	row2 = _mm256_permute2f128_pd(tmp0, tmp1, 0x31);   
	row3 = _mm256_permute2f128_pd(tmp2, tmp3, 0x31);
}
void transposeMatrix_4x4(double* matrix) {
	__m256d x1 = _mm256_load_pd(&matrix[0]);
	__m256d x2 = _mm256_load_pd(&matrix[4]);
	__m256d x3 = _mm256_load_pd(&matrix[8]);
	__m256d x4 = _mm256_load_pd(&matrix[12]);

	transpose8_pd(x1, x2, x3, x4);
	_mm256_store_pd(&matrix[0], x1);
	_mm256_store_pd(&matrix[4], x2);
	_mm256_store_pd(&matrix[8], x3);
	_mm256_store_pd(&matrix[12], x4);
}
double** reshapeFilters(double* Filters, int n_f, int in_CH, int k_H, int k_W) {
	int lenghtCol = (in_CH) * (k_H) * (k_W);
	int r = lenghtCol % 8;
	if (r) {
		lenghtCol += (8 - r);
	}
	double** filters = (double**)malloc(n_f * sizeof(double*));

	for (int j = 0; j < n_f; j++) {

		filters[j] = (double*)malloc(lenghtCol * sizeof(double));
	}
	int b = 0;

	for (int j = 0; j < n_f; j++) {

		for (int i = 0; i < in_CH * k_H * k_W; i++) {
			filters[j][i] = Filters[b];
			b++;

		}
	}
	return filters;

}
void convolve_Im2col_simple_SIMD_AVX_MultiThread_4_4_TransposeSUM_double(double* res, double* input, double* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	double** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow, S);
	double** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);

	auto f = [](double* res, double** imTcol, double** filters, int o_CH, int W_out, int H_out, int lengthRow, int lenghtCol, int N_thread, int Thread_ID) {
		for (int i = Thread_ID; i < o_CH; i += N_thread) {
			for (int j = 0; j < lengthRow; j += 4) {
				__m256d x;
				__m256d y1;
				__m256d y2;
				__m256d y3;
				__m256d y4;
				__m256d acc1;
				__m256d acc2;
				__m256d acc3;
				__m256d acc4;

				acc1 = _mm256_setzero_pd();
				acc2 = _mm256_setzero_pd();
				acc3 = _mm256_setzero_pd();
				acc4 = _mm256_setzero_pd();

				for (int k = 0; k < lenghtCol; k += 4) {
					x = _mm256_load_pd(&filters[i][k]);
					y1 = _mm256_load_pd(&imTcol[j][k]);
					y2 = _mm256_load_pd(&imTcol[j + 1][k]);
					y3 = _mm256_load_pd(&imTcol[j + 2][k]);
					y4 = _mm256_load_pd(&imTcol[j + 3][k]);

					acc1 = _mm256_fmadd_pd(x, y1, acc1);
					acc2 = _mm256_fmadd_pd(x, y2, acc2);
					acc3 = _mm256_fmadd_pd(x, y3, acc3);
					acc4 = _mm256_fmadd_pd(x, y4, acc4);


				}

				transpose8_pd(acc1, acc2, acc3, acc4);
				acc1 = _mm256_add_pd(acc1, acc2);
				acc1 = _mm256_add_pd(acc1, acc3);
				acc1 = _mm256_add_pd(acc1, acc4);



				int l = j + 4 - W_out * H_out;
				if (l < 0) {
					_mm256_store_pd(&res[i * W_out * H_out + j], acc1);
				}
				else {
					for (int k = 0; k < 4 - l; k++) {
						res[i * W_out * H_out + j + k] = acc1.m256d_f64[k];

					}
				}

			}
		}
	};
	thread* th = new thread[N_thread];
	for (int i = 0; i < N_thread; i++) {
		th[i] = thread(f, res, imTcol, filters, o_CH, W_out, H_out, lengthRow, lenghtCol, N_thread, i);
	}
	for (int i = 0; i < N_thread; i++) {
		th[i].join();
	}
	free(imTcol);
	free(filters);
}
void convolve_Im2col_Simple_MultiThread_double(double* res, double* input, double* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	double** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow, S);
	double** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);
	auto f = [](double* res, double** imTcol, double** filters, int o_CH, int W_out, int H_out, int lengthRow, int lenghtCol, int N_thread, int Thread_ID) {
		for (int i = Thread_ID; i < o_CH; i += N_thread) {
			for (int j = 0; j < lengthRow; j++) {
				for (int k = 0; k < lenghtCol; k++) {
					res[i * W_out * H_out + j] += imTcol[j][k] * filters[i][k];
				}
			}
		}
	};
	thread* th = new thread[N_thread];
	for (int i = 0; i < N_thread; i++) {
		th[i] = thread(f, res, imTcol, filters, o_CH, W_out, H_out, lengthRow, lenghtCol, N_thread, i);
	}
	for (int i = 0; i < N_thread; i++) {
		th[i].join();
	}
	free(imTcol);
	free(filters);
}

void convolve_Im2col_simple_SIMD_AVX_MultiThread_8_4_fastSUM_double(double* res, double* input, double* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	double** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow, S);
	double** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);

	auto f = [](double* res, double** imTcol, double** filters, int o_CH, int W_out, int H_out, int lengthRow, int lenghtCol, int N_thread, int Thread_ID) {
		for (int i = Thread_ID; i < o_CH; i += N_thread) {
			for (int j = 0; j < lengthRow; j += 8) {
				__m256d x;
				__m256d y1;
				__m256d y2;
				__m256d y3;
				__m256d y4;
				__m256d y5;
				__m256d y6;
				__m256d y7;
				__m256d y8;
				__m256d acc1;
				__m256d acc2;
				__m256d acc3;
				__m256d acc4;
				__m256d acc5;
				__m256d acc6;
				__m256d acc7;
				__m256d acc8;
				acc1 = _mm256_setzero_pd();
				acc2 = _mm256_setzero_pd();
				acc3 = _mm256_setzero_pd();
				acc4 = _mm256_setzero_pd();
				acc5 = _mm256_setzero_pd();
				acc6 = _mm256_setzero_pd();
				acc7 = _mm256_setzero_pd();
				acc8 = _mm256_setzero_pd();
				for (int k = 0; k < lenghtCol; k += 4) {
					x = _mm256_load_pd(&filters[i][k]);
					y1 = _mm256_load_pd(&imTcol[j][k]);
					y2 = _mm256_load_pd(&imTcol[j + 1][k]);
					y3 = _mm256_load_pd(&imTcol[j + 2][k]);
					y4 = _mm256_load_pd(&imTcol[j + 3][k]);
					y5 = _mm256_load_pd(&imTcol[j + 4][k]);
					y6 = _mm256_load_pd(&imTcol[j + 5][k]);
					y7 = _mm256_load_pd(&imTcol[j + 6][k]);
					y8 = _mm256_load_pd(&imTcol[j + 7][k]);
					acc1 = _mm256_fmadd_pd(x, y1, acc1);
					acc2 = _mm256_fmadd_pd(x, y2, acc2);
					acc3 = _mm256_fmadd_pd(x, y3, acc3);
					acc4 = _mm256_fmadd_pd(x, y4, acc4);
					acc5 = _mm256_fmadd_pd(x, y5, acc5);
					acc6 = _mm256_fmadd_pd(x, y6, acc6);
					acc7 = _mm256_fmadd_pd(x, y7, acc7);
					acc8 = _mm256_fmadd_pd(x, y8, acc8);

				}

				res[i * W_out * H_out + j] = sum8_double(acc1);
				if (j + 7 < W_out * H_out) {
					res[i * W_out * H_out + j + 7] = sum8_double(acc8);
					res[i * W_out * H_out + j + 6] = sum8_double(acc7);
					res[i * W_out * H_out + j + 5] = sum8_double(acc6);
					res[i * W_out * H_out + j + 4] = sum8_double(acc5);
					res[i * W_out * H_out + j + 3] = sum8_double(acc4);
					res[i * W_out * H_out + j + 2] = sum8_double(acc3);
					res[i * W_out * H_out + j + 1] = sum8_double(acc2);
				}
				else if (j + 6 < W_out * H_out) {
					res[i * W_out * H_out + j + 6] = sum8_double(acc7);
					res[i * W_out * H_out + j + 5] = sum8_double(acc6);
					res[i * W_out * H_out + j + 4] = sum8_double(acc5);
					res[i * W_out * H_out + j + 3] = sum8_double(acc4);
					res[i * W_out * H_out + j + 2] = sum8_double(acc3);
					res[i * W_out * H_out + j + 1] = sum8_double(acc2);
				}
				else if (j + 5 < W_out * H_out) {
					res[i * W_out * H_out + j + 5] = sum8_double(acc6);
					res[i * W_out * H_out + j + 4] = sum8_double(acc5);
					res[i * W_out * H_out + j + 3] = sum8_double(acc4);
					res[i * W_out * H_out + j + 2] = sum8_double(acc3);
					res[i * W_out * H_out + j + 1] = sum8_double(acc2);
				}
				else if (j + 4 < W_out * H_out) {
					res[i * W_out * H_out + j + 4] = sum8_double(acc5);
					res[i * W_out * H_out + j + 3] = sum8_double(acc4);
					res[i * W_out * H_out + j + 2] = sum8_double(acc3);
					res[i * W_out * H_out + j + 1] = sum8_double(acc2);
				}
				else if (j + 3 < W_out * H_out) {
					res[i * W_out * H_out + j + 3] = sum8_double(acc4);
					res[i * W_out * H_out + j + 2] = sum8_double(acc3);
					res[i * W_out * H_out + j + 1] = sum8_double(acc2);
				}
				else if (j + 2 < W_out * H_out) {
					res[i * W_out * H_out + j + 2] = sum8_double(acc3);
					res[i * W_out * H_out + j + 1] = sum8_double(acc2);
				}
				else if (j + 1 < W_out * H_out) {
					res[i * W_out * H_out + j + 1] = sum8_double(acc2);
				}
			}
		}
	};
	thread* th = new thread[N_thread];
	for (int i = 0; i < N_thread; i++) {
		th[i] = thread(f, res, imTcol, filters, o_CH, W_out, H_out, lengthRow, lenghtCol, N_thread, i);
	}
	for (int i = 0; i < N_thread; i++) {
		th[i].join();
	}
	free(imTcol);
	free(filters);
}
void convolve_Im2col_simple_SIMD_AVX_MultiThread_2_4_fastSUM_double(double* res, double* input, double* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread) {
	int lenghtCol = 0, lengthRow;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;
	double** imTcol = im2col(input, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow, S);
	double** filters = reshapeFilters(Filters, o_CH, in_CH, k_H, k_W);

	auto f = [](double* res, double** imTcol, double** filters, int o_CH, int W_out, int H_out, int lengthRow, int lenghtCol, int N_thread, int Thread_ID) {
		for (int i = Thread_ID; i < o_CH; i += N_thread) {
			for (int j = 0; j < lengthRow; j += 2) {
				__m256d x;
				__m256d y;
				__m256d y2;
				__m256d acc;
				__m256d acc2;
				acc = _mm256_setzero_pd();
				acc2 = _mm256_setzero_pd();
				for (int k = 0; k < lenghtCol; k += 4) {
					x = _mm256_load_pd(&filters[i][k]);
					y = _mm256_load_pd(&imTcol[j][k]);
					y2 = _mm256_load_pd(&imTcol[j + 1][k]);
					acc = _mm256_fmadd_pd(x, y, acc);
					acc2 = _mm256_fmadd_pd(x, y2, acc2);

				}
				res[i * W_out * H_out + j] = sum8_double(acc);

				if (j + 1 < W_out * H_out) {
					res[i * W_out * H_out + j + 1] = sum8_double(acc2);
				}
			}
		}
	};
	thread* th = new thread[N_thread];
	for (int i = 0; i < N_thread; i++) {
		th[i] = thread(f, res, imTcol, filters, o_CH, W_out, H_out, lengthRow, lenghtCol, N_thread, i);
	}
	for (int i = 0; i < N_thread; i++) {
		th[i].join();
	}
	free(imTcol);
	free(filters);
}
/*
void setZero(float* table, int lenght) {
	for (int i = 0; i < lenght; i++) {
		table[i] = 0;
	}
}
int main()
{
	//float* M = new float[64];
	//for (int i = 0; i < 64; i++) {
	//	M[i] = i%8;
	//	cout << M[i] << " ";
	//	if ((i + 1) % 8 == 0) {
	//		cout << endl;
	//	}
	//}
	//transposeMatrix_8x8(M);
	//for (int i = 0; i < 64; i++) {
	//	cout << M[i] << " ";
	//	if ((i+1) % 8 == 0) {
	//		cout << endl;
	//	}
	//}
	//im2ColExport(res,input,  in_CH,  in_W,  in_H, 3, 3, 0, 1);
	//im2ColExport

	clock_t t;

	const int in_CH =64;
	const int in_W = 256;
	const int in_H = 256;
	const int o_CH = 512;
	const int k_W = 3;
	const int k_H = 3;

	int P = 0;
	int S = 1;
	const int W_out = (in_W - k_W + 2 * P) / S + 1;
	const int H_out = (in_H - k_H + 2 * P) / S + 1;
	const int nCol = W_out * H_out;

	float * arr= (float*)malloc(in_CH *in_W*in_H* sizeof(float));
	int l = 0;
	for (int j = 0; j < in_CH ; j++) {
		int w = 0;
		for (int i = 0; i < in_W; i++) {
			for (int k = 0; k < in_H; k++) {
				arr[l] = 1;// j* in_W* in_H + i * in_W + k;
				l++;
				w++;
			}
		}
		
	}

	float* filters = (float*)malloc(o_CH*in_CH * k_H * k_W * sizeof(float));
	for (int j = 0; j < o_CH * in_CH * k_W * k_H; j++) {
		filters[j] = 1;
	}

	int lenghtCol = 0, lengthRow;
	float** imTcol = im2col(arr, in_CH, in_W, in_H, W_out, H_out, k_W, k_H, &lenghtCol, &lengthRow);

	for (int i = 0; i < lengthRow; i++) {
		for (int j = 0; j < lenghtCol; j++) {
			//cout << imTcol[i][j] << " ";
		}
		//cout << endl;
	}

	float * result= (float*)malloc(o_CH * W_out * H_out * sizeof(float));
	setZero(result, o_CH * W_out * H_out);
	
	t = clock();
	//convolveSimple(result, arr, filters, in_CH, in_W, in_H, o_CH, k_W, k_H, P, S);
	t = clock() - t;
	setZero(result, o_CH * W_out * H_out);
	printf("convolveSimple %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);
	
	t = clock();
	//convolveSimple_MultiThread(result, arr, filters, in_CH, in_W, in_H, o_CH, k_W, k_H, P, S,8);
	t = clock() - t;
	setZero(result, o_CH * W_out * H_out);
	printf("convolveSimple_MultiThread %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);

	t = clock();
	//convolve_Im2col_Simple(result, arr, filters, in_CH, in_W, in_H, o_CH, k_W, k_H, P, S);
	t = clock() - t;
	setZero(result, o_CH * W_out * H_out);
	printf("convolve_Im2col_Simple %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);
	
	t = clock();
	//convolve_Im2col_Simple_MultiThread(result, arr, filters, in_CH, in_W, in_H, o_CH, k_W, k_H, P, S,8);
	t = clock() - t;
	setZero(result, o_CH * W_out * H_out);
	printf("convolve_Im2col_Simple_MultiThread %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);
	
	t = clock();
	//convolve_Im2col_simple_SIMD_AVX(result, arr, filters, in_CH, in_W, in_H, o_CH, k_W, k_H, P, S);
	t = clock() - t;
	setZero(result, o_CH * W_out * H_out);
	printf("convolve_Im2col_simple_SIMD_AVX %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);
	
	t = clock();
	convolve_Im2col_simple_SIMD_AVX_MultiThread(result, arr, filters, in_CH, in_W, in_H, o_CH, k_W, k_H, P, S,4);
	t = clock() - t;
	setZero(result, o_CH * W_out * H_out);
	printf("convolve_Im2col_simple_SIMD_AVX_MultiThread %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);

	t = clock();
	convolve_Im2col_simple_SIMD_AVX_MultiThread_2_8(result, arr, filters, in_CH, in_W, in_H, o_CH, k_W, k_H, P, S, 4);
	t = clock() - t;
	setZero(result, o_CH * W_out * H_out);
	printf("convolve_Im2col_simple_SIMD_AVX_MultiThread_2_8 %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);
	
	t = clock();
	convolve_Im2col_simple_SIMD_AVX_MultiThread_2_8_fastSUM(result, arr, filters, in_CH, in_W, in_H, o_CH, k_W, k_H, P, S, 4);
	t = clock() - t;
	setZero(result, o_CH * W_out * H_out);
	printf("convolve_Im2col_simple_SIMD_AVX_MultiThread_2_8_fastSUM %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);

	t = clock();
	convolve_Im2col_simple_SIMD_AVX_MultiThread_4_8_fastSUM(result, arr, filters, in_CH, in_W, in_H, o_CH, k_W, k_H, P, S, 4);
	t = clock() - t;
	setZero(result, o_CH * W_out * H_out);
	printf("convolve_Im2col_simple_SIMD_AVX_MultiThread_4_8_fastSUM %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);

	t = clock();
	convolve_Im2col_simple_SIMD_AVX_MultiThread_8_8_fastSUM(result, arr, filters, in_CH, in_W, in_H, o_CH, k_W, k_H, P, S, 4);
	t = clock() - t;
	setZero(result, o_CH * W_out * H_out);
	printf("convolve_Im2col_simple_SIMD_AVX_MultiThread_8_8_fastSUM %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);

	t = clock();
	convolve_Im2col_simple_SIMD_AVX_MultiThread_8_8_TransposeSUM(result, arr, filters, in_CH, in_W, in_H, o_CH, k_W, k_H, P, S, 4);
	t = clock() - t;
	//setZero(result, o_CH * W_out * H_out);
	printf("convolve_Im2col_simple_SIMD_AVX_MultiThread_8_8_TransposeSUM %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);
	for (int i = 0; i < o_CH * W_out * H_out; i++) {
		cout << result[i] << endl;
	}

    return 0;
}
*/