#define EXTERN_DLL_EXPORT extern "C" __declspec(dllexport)  


EXTERN_DLL_EXPORT void convolve_Im2col_simple_SIMD_AVX_MultiThread_8_8_TransposeSUM(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
EXTERN_DLL_EXPORT void convolve_Im2col_simple_SIMD_AVX_MultiThread_8_8_fastSUM(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
EXTERN_DLL_EXPORT void convolve_Im2col_simple_SIMD_AVX_MultiThread_4_8_fastSUM(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
EXTERN_DLL_EXPORT void convolve_Im2col_simple_SIMD_AVX_MultiThread_2_8_fastSUM(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
EXTERN_DLL_EXPORT void convolve_Im2col_simple_SIMD_AVX_MultiThread_2_8(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
EXTERN_DLL_EXPORT void convolve_Im2col_simple_SIMD_AVX_MultiThread(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
EXTERN_DLL_EXPORT void convolve_Im2col_simple_SIMD_AVX(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S);
EXTERN_DLL_EXPORT void convolve_Im2col_Simple_MultiThread(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
EXTERN_DLL_EXPORT void convolveSimple_MultiThread(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
EXTERN_DLL_EXPORT void convolveSimple(float* res, float* input, float* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S);
EXTERN_DLL_EXPORT void im2ColExport(float* res, float* input, int in_CH, int in_W, int in_H, int K_W, int K_H, int P, int S);
EXTERN_DLL_EXPORT void transposeMatrix_8x8(float* matrix);
EXTERN_DLL_EXPORT void transposeMatrix_4x4(double* matrix);
EXTERN_DLL_EXPORT void convolve_Im2col_simple_SIMD_AVX_MultiThread_4_4_TransposeSUM_double(double* res, double* input, double* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
EXTERN_DLL_EXPORT void convolve_Im2col_Simple_MultiThread_double(double* res, double* input, double* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
EXTERN_DLL_EXPORT void convolve_Im2col_simple_SIMD_AVX_MultiThread_8_4_fastSUM_double(double* res, double* input, double* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
EXTERN_DLL_EXPORT void convolve_Im2col_simple_SIMD_AVX_MultiThread_2_4_fastSUM_double(double* res, double* input, double* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);