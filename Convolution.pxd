cdef extern from "Convolution.h":
	void convolve_Im2col_simple_SIMD_AVX_MultiThread_8_8_TransposeSUM(float* res, float* input,float* filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
	void convolve_Im2col_simple_SIMD_AVX_MultiThread_8_8_fastSUM(float* res, float* input,float* filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
	void convolve_Im2col_simple_SIMD_AVX_MultiThread_4_8_fastSUM(float* res, float* input,float* filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
	void convolve_Im2col_simple_SIMD_AVX_MultiThread_2_8_fastSUM(float* res, float* input,float* filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
	void convolve_Im2col_simple_SIMD_AVX_MultiThread_2_8(float* res, float* input,float* filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
	void convolve_Im2col_simple_SIMD_AVX_MultiThread(float* res, float* input,float* filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
	void convolve_Im2col_simple_SIMD_AVX(float* res, float* input,float* filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S);
	void convolve_Im2col_Simple_MultiThread(float* res, float* input,float* filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
	void convolveSimple_MultiThread(float* res, float* input,float* filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
	void convolveSimple(float* res, float* input,float* filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S);
	void im2ColExport(float* res, float* input, int in_CH, int in_W, int in_H, int k_W, int k_H, int P, int S);
	void transposeMatrix_8x8(float* matrix);
	void convolve_Im2col_simple_SIMD_AVX_MultiThread_4_4_TransposeSUM_double(double* res, double* input,double* filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
	void convolve_Im2col_Simple_MultiThread_double(double* res, double* input,double* filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
	void transposeMatrix_4x4(double* matrix);
	void convolve_Im2col_simple_SIMD_AVX_MultiThread_8_4_fastSUM_double(double* res, double* input, double* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
	void convolve_Im2col_simple_SIMD_AVX_MultiThread_2_4_fastSUM_double(double* res, double* input, double* Filters, int in_CH, int in_W, int in_H, int o_CH, int k_W, int k_H, int P, int S, int N_thread);
