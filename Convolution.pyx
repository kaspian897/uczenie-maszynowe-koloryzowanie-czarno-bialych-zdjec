cimport Convolution 
import numpy as np  
cimport numpy as np  

from cpython.pycapsule cimport *

def convolveIC_AVX_8x8_MT_TS(np.ndarray[np.float32_t, ndim=3] result,np.ndarray[np.float32_t, ndim=3] input,np.ndarray[np.float32_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S,int N_thread):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolve_Im2col_simple_SIMD_AVX_MultiThread_8_8_TransposeSUM(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S,N_thread)

def convolveIC_AVX_8x8_MT_FS(np.ndarray[np.float32_t, ndim=3] result,np.ndarray[np.float32_t, ndim=3] input,np.ndarray[np.float32_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S,int N_thread):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolve_Im2col_simple_SIMD_AVX_MultiThread_8_8_fastSUM(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S,N_thread)

def convolveIC_AVX_4x8_MT(np.ndarray[np.float32_t, ndim=3] result,np.ndarray[np.float32_t, ndim=3] input,np.ndarray[np.float32_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S,int N_thread):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolve_Im2col_simple_SIMD_AVX_MultiThread_4_8_fastSUM(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S,N_thread)

def convolveIC_AVX_2x8_MT_FS(np.ndarray[np.float32_t, ndim=3] result,np.ndarray[np.float32_t, ndim=3] input,np.ndarray[np.float32_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S,int N_thread):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolve_Im2col_simple_SIMD_AVX_MultiThread_2_8_fastSUM(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S,N_thread)

def convolveIC_AVX_2x8_MT(np.ndarray[np.float32_t, ndim=3] result,np.ndarray[np.float32_t, ndim=3] input,np.ndarray[np.float32_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S,int N_thread):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolve_Im2col_simple_SIMD_AVX_MultiThread_2_8(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S,N_thread)

def convolveIC_AVX_MT(np.ndarray[np.float32_t, ndim=3] result,np.ndarray[np.float32_t, ndim=3] input,np.ndarray[np.float32_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S,int N_thread):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolve_Im2col_simple_SIMD_AVX_MultiThread(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S,N_thread)

def convolveIC_AVX(np.ndarray[np.float32_t, ndim=3] result,np.ndarray[np.float32_t, ndim=3] input,np.ndarray[np.float32_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolve_Im2col_simple_SIMD_AVX(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S)

def convolveIC_MT(np.ndarray[np.float32_t, ndim=3] result,np.ndarray[np.float32_t, ndim=3] input,np.ndarray[np.float32_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S,int N_thread):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolve_Im2col_Simple_MultiThread(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S,N_thread)

def convolve_MT(np.ndarray[np.float32_t, ndim=3] result,np.ndarray[np.float32_t, ndim=3] input,np.ndarray[np.float32_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S,int N_thread):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolveSimple_MultiThread(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S,N_thread)

def convolve_Naive(np.ndarray[np.float32_t, ndim=3] result,np.ndarray[np.float32_t, ndim=3] input,np.ndarray[np.float32_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolveSimple(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S)

def im2Col(np.ndarray[np.float32_t, ndim=2] result,np.ndarray[np.float32_t, ndim=3] input,int in_CH,int in_W,int in_H,int k_W,int k_H,int P,int S):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.im2ColExport(&result[0,0],&input[0,0,0], in_CH, in_W, in_H,k_W,k_H,0,S)

def transpose_Matrix_8x8(np.ndarray[np.float32_t, ndim=2] matrix):
	Convolution.transposeMatrix_8x8(&matrix[0,0])
def transpose_Matrix_4x4(np.ndarray[np.float64_t, ndim=2] matrix):
	Convolution.transposeMatrix_4x4(&matrix[0,0])
	
def convolveIC_MT_D(np.ndarray[np.float64_t, ndim=3] result,np.ndarray[np.float64_t, ndim=3] input,np.ndarray[np.float64_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S,int N_thread):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolve_Im2col_Simple_MultiThread_double(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S,N_thread)
	
def convolveIC_AVX_4x4_MT_TS_D(np.ndarray[np.float64_t, ndim=3] result,np.ndarray[np.float64_t, ndim=3] input,np.ndarray[np.float64_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S,int N_thread):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolve_Im2col_simple_SIMD_AVX_MultiThread_4_4_TransposeSUM_double(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S,N_thread)

def convolveIC_AVX_8x4_MT_TS_D(np.ndarray[np.float64_t, ndim=3] result,np.ndarray[np.float64_t, ndim=3] input,np.ndarray[np.float64_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S,int N_thread):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolve_Im2col_simple_SIMD_AVX_MultiThread_8_4_fastSUM_double(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S,N_thread)
	
def convolveIC_AVX_2x4_MT_TS_D(np.ndarray[np.float64_t, ndim=3] result,np.ndarray[np.float64_t, ndim=3] input,np.ndarray[np.float64_t, ndim=4] Filters,int in_CH,int in_W,int in_H,int o_CH,int k_W,int k_H,int P,int S,int N_thread):
	input=np.pad(input ,P,constant_values=(0))[P:(in_CH+P)]
	in_H=in_H+2*P
	in_W=in_W+2*P
	Convolution.convolve_Im2col_simple_SIMD_AVX_MultiThread_2_4_fastSUM_double(&result[0,0,0],&input[0,0,0], &Filters[0,0,0,0], in_CH, in_W, in_H,o_CH,k_W,k_H,0,S,N_thread)
	


	

    
